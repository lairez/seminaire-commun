##
# The Joint Specfun-Polsys seminar
#

TODO=index.html style.css

UPLOAD=$(TODO) extra archives

export LC_LANG="en_US.UTF-8"
export LC_TIME="en_US.UTF-8"

.PHONY: clean

all: $(TODO)

index.html: index.org context.el
	emacs $< --batch -l context.el -f org-html-export-to-html --kill

style.css: style.scss
	sassc -t compact $< > $@

deploy: all
	rsync --recursive --verbose $(UPLOAD) mathexc@ssh.cluster031.hosting.ovh.net:www/seminar

clean:
	rm -rf $(TODO)
